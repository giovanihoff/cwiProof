/**
 * @author COSTA, Giovani Hoff <giovanihoff@gmail.com>
 */

const path = require('path');
const reporter = require('cucumber-html-reporter');

var {defineSupportCode} = require('cucumber');
var fs = require('fs');
var outputDir = path.resolve( __dirname + '/report')
defineSupportCode(function({setDefaultTimeout}) {
 setDefaultTimeout(120 * 1000);
  if (!fs.existsSync(outputDir)) {
     fs.mkdirSync(outputDir);
  }
});

exports.config = {

  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      prefs: { "profile.managed_default_content_settings.images": 2 },
      args: [ "--headless", "--disable-gpu", "--window-size=1366,900" ]
    }
  },

  params: {
    url: {
      cwi: 'https://www.cwi.com.br/'
    },
    technicalManager: {
      email: 'giovanihoff@gmail.com'
    },
  },

  specs: [
    __dirname + '/features/specifications/functional.feature',
    __dirname + '/features/specifications/service.feature',
  ],

  cucumberOpts: {
    format: ['pretty', 'json:report/cucumber.json'],
    require: [
      __dirname + '/features/step_definitions/*.js',
    ]
  },

  onPrepare: function () {
    browser.driver.manage().window().maximize();
  },

  onComplete: function () {
    var options = {
      theme: 'bootstrap',
      jsonFile: path.resolve(__dirname + '/report/cucumber.json'),
      output: path.resolve(__dirname + '/report/cucumber.html'),
      storeScreenshots: undefined,
      reportSuiteAsScenarios: true,
      launchReport: true,
      brandTitle: 'CWI Software',
      metadata: {
        "Test Environment": browser.params.url.cwi,
        "Technical Manager": browser.params.technicalManager.email
      }
    };
    reporter.generate(options);
  },
  seleniumAddress: 'http://localhost:4444/wd/hub',
  framework: 'custom',
  resultJsonOutputFile: __dirname + '/report/result.json',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  allScriptsTimeout: (180 * 1000),
}
