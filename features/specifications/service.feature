Feature: UI Automation / Service

    Scenario Outline: Star Wars API - Root <root> - Test <test>
         When I get the root "<root>" and number "<number>"
         Then The value of the attibute "model" is "<model>"

        Examples:
        | test | root      | number | model                       |
        | 04   | starships | 9      | DS-1 Orbital Battle Station |

    Scenario Outline: Star Wars API - Root <root> - Test <test>
         When I search "<value>" in the root "<root>"
          And I get the value from attribute "<attribute>"
         Then The response is equal with values from "<file>"

        Examples:
        | test | root  | value      | attribute  | file                      |
        | 03   | films | A New Hope | characters | a_new_hope_characters.txt |

    Scenario Outline: Star Wars API - Root <root> - Test <test>
         When I get the page "<page>" and root "<root>"
          And I get the all values from attribute "<attribute>"

        Examples:
        | test | page | root  | attribute  |
        | 02   | 1    | films | title      |

    Scenario Outline: Star Wars API - Root <root> - Test <test> - Planet <number> - File <file>
         When I get the root "<root>" and number "<number>"
         Then The JSON response is equal with values from "<file>"

        Examples:
        | test | root    | number | file          |
        | 01   | planets | 1      | tatooine.json |
        | 01   | planets | 2      | alderaan.json |
        | 01   | planets | 3      | yaviniv.json  |
        | 01   | planets | 4      | hoth.json     |
        | 01   | planets | 5      | dagobah.json  |
        | 01   | planets | 6      | bespin.json   |
