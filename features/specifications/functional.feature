Feature: UI Automation / Functional

    Scenario: CWI Home Page - Contact Form - Captcha
            # Access CWI Contact
        Given I am on CWI home page
         When I click in XPATH "//*[@id='mainNav']/li[6]/a"
          And I take a screenshot
         Then I will see the title page "Contato — CWI Software"
            # Complete Registration
         When I include the value "Test Subject" in the object ID "Contact_Subject"
          And I include the value "Test Name" in the object ID "Contact_Name"
          And I include the value "Test Company" in the object ID "Contact_Company"
          And I include the value "giovanihoff@gmail.com" in the object ID "Contact_Email"
          And I include the value "51985783092" in the object ID "contact-phone"
          And I include the value "Test Address" in the object ID "Contact_Address"
          And I include the value "Test City" in the object ID "Contact_City"
          And I click in ID "select-state_iconselect"
          And I select the value "RS" in the option ID "select-state_options"
          And I include the value "Test Message" in the object ID "Contact_Message"
            # Submission without Captcha
          And I click in ID "sendSubmit"
          And I take a screenshot
         Then I will see in ID "request-result" the message "Captcha inválido! Tente novamente."
