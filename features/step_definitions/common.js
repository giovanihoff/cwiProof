/**
 * @author COSTA, Giovani Hoff <giovanihoff@gmail.com>
 */

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = chai.assert;
const expect = chai.expect;
chai.use(chaiAsPromised);
const { defineSupportCode } = require('cucumber');
const wtime = 5 * 5000;

defineSupportCode(function ({ Given, When, Then }) {

  When(/^I click in XPATH \"([^\"]*)\"$/, function (object) {
    var EC = protractor.ExpectedConditions;
    browser.wait(EC.elementToBeClickable(element(by.xpath(object))), wtime);
    return element(by.xpath(object)).click();
  });

  When(/^I include the value \"([^\"]*)\" in the object ID \"([^\"]*)\"$/, function (value, object) {
    var EC = protractor.ExpectedConditions;
    browser.wait(EC.presenceOf(element(by.id(object))), wtime);
    return element(by.id(object)).sendKeys(value);
  });

  When(/^I select the value \"([^\"]*)\" in the option ID \"([^\"]*)\"$/, function (value, object) {
    var EC = protractor.ExpectedConditions;
    browser.wait(EC.presenceOf(element(by.id(object))), wtime);
    return element(by.id(object)).element(by.cssContainingText('div', value)).click();
  });

  When(/^I click in ID \"([^\"]*)\"$/, function (object) {
    var EC = protractor.ExpectedConditions;
    browser.wait(EC.elementToBeClickable(element(by.id(object))), wtime);
    return element(by.id(object)).click();
  });

  When(/^I take a screenshot$/, function () {
    var world = this;
    browser.takeScreenshot().then(function (buffer) {
      return world.attach(buffer, 'image/png');
    });
  });

  Then(/^I will see the title page \"([^\"]*)\"$/, function (text) {
    var title = browser.getTitle();
    return expect(title).to.eventually.contain(text);
  });

  Then(/^I will see in ID \"([^\"]*)\" the message \"([^\"]*)\"$/, function (object, message) {
    var EC = protractor.ExpectedConditions;
    browser.wait(EC.presenceOf(element.all(by.id(object))), wtime);
    var name = element.all(by.id(object)).first().getText();
    return expect(name).to.eventually.contain(message);
  });

});
