/**
 * @author COSTA, Giovani Hoff <giovanihoff@gmail.com>
 */

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = chai.assert;
const expect = chai.expect;
chai.use(chaiAsPromised);
const {defineSupportCode} = require('cucumber');
const wtime = 5 * 5000;

defineSupportCode( function({Given, When, Then}) {

  Given(/^I am on CWI home page/, function () {
    browser.waitForAngularEnabled(false);
    browser.get(browser.params.url.cwi);
    var title = browser.getTitle();
    return expect(title).to.eventually.contain('CWI Software');
  });

});
