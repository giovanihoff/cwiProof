/**
 * @author COSTA, Giovani Hoff <giovanihoff@gmail.com>
 */

const fs = require('fs');
const path = require('path');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = chai.assert;
const expect = chai.expect;
chai.use(chaiAsPromised);
const {defineSupportCode} = require('cucumber');
const swapi = require('swapi-node');
const wtime = 5 * 5000;

var deepEqual = require('deep-equal');

var response = null;
var rvalue = null;

defineSupportCode( function({Given, When, Then}) {

  When(/^I get the starship \"([^\"]*)\"/, async function (starship) {
    var result = false;
    try {
      await swapi.get('http://swapi.co/api/starships?search=' + starship).then((result) => {
          response = result;
      });
      result = true;
    } catch (err) {
      console.error(err.message);
    }
    expect(true).to.equal(result);
  });

  When(/^I get the page \"([^\"]*)\", root \"([^\"]*)\" and number \"([^\"]*)\"/, async function (page, root, number) {
    response = null;
    var result = false;
    var request = "http://swapi.co/api/" + root + "/?page=" + page;
    try {
      await swapi.get(request).then((result) => {
          response = result.results[parseInt(number)-1];
      });
      result = true;
    } catch (err) {
      console.error(err.message);
    }
    expect(true).to.equal(result);
  });

  When(/^I get the root \"([^\"]*)\" and number \"([^\"]*)\"/, async function (root, number) {
    response = null;
    var result = false;
    var request = "http://swapi.co/api/" + root + "/" + number;
    try {
      await swapi.get(request).then((result) => {
          response = result;
      });
      result = true;
    } catch (err) {
      console.error(err.message);
    }
    expect(true).to.equal(result);
  });

  When(/^I get the page \"([^\"]*)\" and root \"([^\"]*)\"/, async function (page, root) {
    response = null;
    var result = false;
    var request = "http://swapi.co/api/" + root + "/?page=" + page;
    try {
      await swapi.get(request).then((result) => {
          response = result.results;
      });
      result = true;
    } catch (err) {
      console.error(err.message);
    }
    expect(true).to.equal(result);
  });

  When(/^I get the all values from attribute \"([^\"]*)\"/, async function (attribute) {
    rvalue = null;
    var result = false;
    try {
      var getFunction = new Function('object', 'return object.' + attribute);
      for(i in response) {
        rvalue = await getFunction(response[i]);
        console.log(rvalue);
      }
      result = true;
    } catch (err) {
      console.error(err.message);
    }
    expect(true).to.equal(result);
  });

  When(/^I search \"([^\"]*)\" in the root \"([^\"]*)\"/, async function (value, root) {
    response = null;
    var result = false;
    var request = "http://swapi.co/api/" + root + "/?search=" + value;
    try {
      await swapi.get(request).then((result) => {
          response = result;
      });
      result = true;
    } catch (err) {
      console.error(err.message);
    }
    expect(true).to.equal(result);
  });

  When(/^I get the value from attribute \"([^\"]*)\"/, async function (attribute) {
    rvalue = null;
    var result = false;
    try {
      var getFunction = new Function('object', 'return object.results[0].' + attribute);
      rvalue = await getFunction(response);
      console.log(rvalue);
      result = true;
    } catch (err) {
      console.error(err.message);
    }
    expect(true).to.equal(result);
  });

  Then(/^The value of the attibute \"([^\"]*)\" is \"([^\"]*)\"/, async function (attribute, value) {
    try {
      var getFunction = new Function('object', 'return object.' + attribute);
      var rvalue = await getFunction(response);
      console.log(rvalue);
    } catch (err) {
      console.error(err.message);
    }
    expect(value).to.equal(rvalue);
  });

  Then(/^The response is equal with values from \"([^\"]*)\"/, function (file) {
    var tFile = ( fs.readFileSync(path.resolve(__dirname + '/../../data/' + file), 'utf8') ).toString();
    var sValue = rvalue.toString();
    tFile = tFile.replace(/(\r\n|\n|\r)/gm,",");
    return assert.deepEqual( sValue, tFile );
  });

  Then(/^The JSON response is equal with values from \"([^\"]*)\"/, function (file) {
    var jfile = JSON.parse(
      fs.readFileSync(path.resolve(__dirname + '/../../data/' + file), 'utf8')
    );
    jfile = JSON.stringify(jfile);
    return assert.deepEqual( JSON.stringify(response), jfile );
  });

});
